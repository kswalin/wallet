json.array!(@expenses) do |expense|
  json.extract! expense, :id, :category_id, :amount
  json.url expense_url(expense, format: :json)
end
