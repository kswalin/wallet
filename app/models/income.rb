class Income < ActiveRecord::Base
  belongs_to :category
  validates :amount, presence: true, numericality: true
end
