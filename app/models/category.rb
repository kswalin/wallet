class Category < ActiveRecord::Base
  has_many :income, dependent: :destroy
  has_many :expense, dependent: :destroy
  validates :name, presence: true
  validates :date, presence: true
end
