class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.references :category, index: true, foreign_key: true
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
